# Batch file transcoder

This utility can be used as a standalone, or imported into your Java programs for more flexibility, in which case please refer to the Javadoc.
This document covers the use of the standalone version.

FileTranscoder is able to recursively iterate over multiple directories and change the encoding of each file **in place**.
The files can be filtered to transcode only those matching a given name.
The original encoding must be known in advance and specified.
Be aware that no checks are made to see if the files are actually in the given encoding!

## Usage
```
java -jar file-transcoder-0.0.1.jar -dirs <directories...> [-filter <filename>] -src <source encoding> -tgt <target encoding>
 -dirs <directories...>   directories where to find files to transcode
 -filter <filename>       only transcode files with the given name
 -src <source encoding>   original encoding of the source files
 -tgt <target encoding>   encoding to which the files will be transcoded
```
For supported encodings and their canonical name, refer to: https://docs.oracle.com/javase/8/docs/technotes/guides/intl/encoding.doc.html

## Examples

To transcode every file in the current working directory from `Latin-1` to `UTF-8`:
```
java -jar file-transcoder-0.0.1.jar -dirs . -src ISO-8859-1 -tgt UTF-8
```

To transcode only files named `foo.properties`:
```
java -jar file-transcoder-0.0.1.jar -dirs . -filter foo.properties -src ISO-8859-1 -tgt UTF-8
```
